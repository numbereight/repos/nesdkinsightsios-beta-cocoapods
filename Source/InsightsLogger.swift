//
//  InsightsLogger.swift
//  Insights
//
//  Created by Chris Watts on 12/09/2022.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

#if os(watchOS)
import WatchKit
#endif

internal class InsightsLogger {

    private static let LOG_TAG = "InsightsLogger"

    private static func getRecorder(forSession session: String) throws -> Recorder {
        return try Recorder(continuing: session)
    }
    
    private static func makeRecorder(session: SessionInfo, config: RecordingConfig) throws -> Recorder {
        var sessionInfo = session
        sessionInfo.deviceId = config.deviceId

        let metadata = try JSONEncoder().encode(sessionInfo)
        let recorder = try Recorder(session: sessionInfo.sessionId, metadata: metadata)

        return recorder
    }
    
    static func makeLogger(forSession session: SessionInfo, defaults: UserDefaults, config: RecordingConfig? = nil) throws -> InsightsLogger {
        let resolvedConfig = config ?? RecordingConfig.default
        let recorder = try self.makeRecorder(session: session, config: resolvedConfig)
        return InsightsLogger(recorder: recorder, defaults: defaults, config: resolvedConfig)
    }
    
    static func sizeOfSession(session: String) -> Int {
        switch Recorder.count(of: session) {
        case let .success(x):
            return x
        case .failure:
            return -1
        }
    }
    
    let defaults: UserDefaults
    private(set) var config: RecordingConfig
    
    let recorder: Recorder
    private(set) var currentState: InsightsState
    
    var currentSession: String {
        return self.recorder.currentSession
    }

    init(recorder: Recorder, defaults: UserDefaults, config: RecordingConfig? = nil) {
        self.recorder = recorder
        self.defaults = defaults

        let resolvedConfig = config ?? RecordingConfig.default
        self.config = resolvedConfig
        self.currentState = InsightsState(mostProbableOnly: resolvedConfig.mostProbableOnly)
    }
    
    func updateConfig(config: RecordingConfig) {
        self.config = config
        self.currentState.mostProbableOnly = config.mostProbableOnly
    }
    
    func erase() -> Swift.Result<Void, Error> {
        return self.recorder.erase()
    }

    func logLifecycle(name: String, timestamp: String) -> Result<Void, Error> {
        let session = self.recorder.currentSession
        return self.logLifecycle(session: session, name: name, timestamp: timestamp)
    }
    
    func logLifecycle(session: String, name: String, timestamp: String) -> Result<Void, Error> {
        let payload = Payload.lifecycle(LifecyclePayload(timestamp: timestamp,
                                                         id: name))
        let result = Result { () -> Data in
            try JSONEncoder().encode(payload)
        }
        switch result {
        case let .failure(error):
            return .failure(error)
        case let .success(data):
            return self.writeSessionDataToDatabase(session: session, data: data, state: self.currentState.currentState, timestamp: timestamp)
        }
    }

    func logMarker(name: String, parameters: [String: Any] = [:]) -> Result<Void, Error> {
        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        return self.logMarker(name: name, parameters: parameters, timestamp: now)
    }

    func logMarker(name: String, parameters: [String: Any], timestamp: String) -> Result<Void, Error> {
        self.addHeartbeat()

        let payload =
            Payload.marker(MarkerPayload(timestamp: timestamp,
                                         id: name,
                                         params: parameters))
        let result = Result { () -> Data in
            try JSONEncoder().encode(payload)
        }
        switch result {
        case let .failure(error):
            return .failure(error)
        case let .success(data):
            return self.writeSessionDataToDatabase(data: data, state: self.currentState.currentState, timestamp: timestamp)
        }
    }
    
    func logEvent(topic: String, glimpse: Glimpse<NEXSensorItem>) {
        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        self.logEvent(topic: topic, glimpse: glimpse, timestamp: now)
    }

    func logEvent(topic: String, glimpse: Glimpse<NEXSensorItem>, timestamp: String) {
        NELog.msg(InsightsLogger.LOG_TAG, verbose: "In Insights Subscribe callback. Got data for insights subscriptions.", metadata: ["topic": topic])

        self.addHeartbeat()

        self.currentState.updateWithEvent(topic: topic, glimpse: glimpse)

        // Update 19/07/21: the receive time is now used instead of the event time
        // to ensure that asynchronous events aren't written before the last upload.
        var data = GlimpsePayload.GlimpseData(from: glimpse, timestamp: timestamp)

        if self.config.mostProbableOnly {
            data.values = [data.values[0]]
        } else {
            // Remove any items with less than 0.1 confidence unless
            // it is the most confident value
            var significantValues = data.values.filter {
                $0.confidence >= 0.1
            }
            if significantValues.count == 0 {
                significantValues.append(data.values[0])
            }

            data.values = significantValues
        }

        let gl = GlimpsePayload(from: data)
        let payload = Payload.glimpse(gl)

        if let data = try? JSONEncoder().encode(payload) {
            NELog.msg(InsightsLogger.LOG_TAG, verbose: "In Insights Subscribe callback. Got Recording payload.", metadata: ["topic": topic])

            switch self.recorder.record(data, state: self.currentState.currentState, at: timestamp) {
            case .success:
                NELog.msg(InsightsLogger.LOG_TAG, verbose: "In Insights Subscribe callback. Finished saving payload.", metadata: ["topic": topic])
            case let .failure(error):
                NELog.msg(InsightsLogger.LOG_TAG, warning: "Failed to record event for topic to database with error.", metadata: ["topic": topic, "error": error.localizedDescription])
            }
        }
    }
    
    func addHeartbeat(timestamp: String = MicrosecondPrecisionDateFormatter().string(from: Date())) {
        self.defaults.set(timestamp, forKey: "lastHeartbeat")
    }
    
    func writeSessionDataToDatabase(data: Data, state: InsightsState.StateType, timestamp: String) -> Result<Void, Error> {
        let session = self.recorder.currentSession
        return self.writeSessionDataToDatabase(session: session, data: data, state: state, timestamp: timestamp)
    }

    func writeSessionDataToDatabase(session: String, data: Data, state: InsightsState.StateType, timestamp: String) -> Result<Void, Error> {
        do {
            let recorder: Recorder
            if self.recorder.currentSession == session {
                recorder = self.recorder
            } else {
                recorder = try InsightsLogger.getRecorder(forSession: session)
            }

            return recorder.record(data, state: state, at: timestamp)
        } catch let error {
            return .failure(error)
        }
    }
}
