//
//  RequestModel.swift
//  Insights
//
//  Created by Oliver Kocsis on 18/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight
import Codability

internal struct RequestBody: Encodable {
    let session: SessionInfo
    let data: [Payload]
    let state: InsightsState.StateType
    let nextStateChange: String?
    
    func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.session, forKey: .session)
        try encCont.encode(self.data, forKey: .data)
        try encCont.encode(self.state, forKey: .state)
        try encCont.encode(self.nextStateChange, forKey: .nextStateChange)
    }
    
    enum CodingKeys: String, CodingKey {
        case session
        case data
        case state
        case nextStateChange
    }
}

internal enum Payload: Codable, Equatable, Hashable {
    case glimpse(GlimpsePayload)
    case lifecycle(LifecyclePayload)
    case marker(MarkerPayload)
    
    enum TypeName: String, Codable {
        case glimpse
        case lifecycle
        case marker
    }
    
    var typeName: TypeName {
        switch self {
        case .glimpse:
            return .glimpse
        case .lifecycle:
            return .lifecycle
        case .marker:
            return .marker
        }
    }
    
    init(from decoder: Decoder) throws {
        
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        
        let typeName = try decCont.decode(TypeName.self, forKey: .type)
        switch typeName {
        case .glimpse:
            let payload = try decCont.decode(GlimpsePayload.self, forKey: .payload)
            self = .glimpse(payload)
        case .lifecycle:
            let payload = try decCont.decode(LifecyclePayload.self, forKey: .payload)
            self = .lifecycle(payload)
        case .marker:
            let payload = try decCont.decode(MarkerPayload.self, forKey: .payload)
            self = .marker(payload)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.typeName, forKey: .type)
        switch self {
        case .glimpse(let payload):
            try encCont.encode(payload, forKey: .payload)
        case .lifecycle(let payload):
            try encCont.encode(payload, forKey: .payload)
        case .marker(let payload):
            try encCont.encode(payload, forKey: .payload)
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case type
        case payload
    }
    
    static func == (lhs: Payload, rhs: Payload) -> Bool {
        if lhs.typeName != rhs.typeName {
            return false
        }
        
        var leftGlimpsePayload: GlimpsePayload?
        var leftLifecyclePayload: LifecyclePayload?
        var leftMarkerPayload: MarkerPayload?
        switch lhs {
        case .glimpse(let payload):
            leftGlimpsePayload = payload
        case .lifecycle(let payload):
            leftLifecyclePayload = payload
        case .marker(let payload):
            leftMarkerPayload = payload
        }
        
        switch rhs {
        case .glimpse(let payload):
            guard let payloadL = leftGlimpsePayload else { return false }
            return payloadL == payload
        case .lifecycle(let payload):
            guard let payloadL = leftLifecyclePayload else { return false }
            return payloadL == payload
        case .marker(let payload):
            guard let payloadL = leftMarkerPayload else { return false }
            return payloadL == payload
        }
    }
}

internal struct LifecyclePayload: Codable, Equatable, Hashable {
    let timestamp: String
    let id: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.timestamp)
        hasher.combine(self.id)
    }
}

internal struct MarkerPayload: Codable, Equatable, Hashable {
    let timestamp: String
    let id: String
    let params: [String: Any]
    
    init(timestamp: String,
         id: String,
         params: [String: Any]) {
        self.timestamp = timestamp
        self.id = id
        self.params = params
    }
    
    init(from decoder: Decoder) throws {
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        
        self.timestamp = try decCont.decode(String.self, forKey: .timestamp)
        self.id = try decCont.decode(String.self, forKey: .id)
        self.params = try decCont.decodeAny(.params)
    }
    
    func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        
        try encCont.encode(self.timestamp, forKey: .timestamp)
        try encCont.encode(self.id, forKey: .id)
        try encCont.encodeAny(self.params, forKey: .params)
    }
    
    enum CodingKeys: String, CodingKey {
        case timestamp
        case id
        case params
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.timestamp)
        hasher.combine(self.id)
        for param in self.params {
            // Use params.keys as a good indication they are the same.
            hasher.combine(param.key)
        }
    }
    
    static func == (lhs: MarkerPayload, rhs: MarkerPayload) -> Bool {
        // Use params.keys as a good indication they are the same.
        return lhs.timestamp == rhs.timestamp &&
        lhs.id == rhs.id &&
        lhs.params.keys == rhs.params.keys
    }
}

internal struct GlimpsePayload: Codable, Equatable, Hashable {
    struct GlimpseData: Codable, Equatable, Hashable {
        struct ValueItem: Codable, Equatable {
            let value: String
            let confidence: Double
        }
        
        var timestamp: String
        var topic: String
        var datatype: String
        var values: [ValueItem]
        
        init(from glimpse: Glimpse<NEXSensorItem>, timestamp: String) {
            self.timestamp = timestamp
            self.topic = glimpse.topic
            let first: NEXSensorItem? = glimpse.mostProbable
            self.datatype = first?.underlyingNETypeName ?? ""
            
            var values = [ValueItem]()
            for possibility in glimpse.possibilities {
                guard let serialized = possibility.serialize() else { continue }
                values.append(GlimpseData.ValueItem(value: serialized, confidence: possibility.confidence))
            }
            self.values = values
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(self.timestamp)
            hasher.combine(self.topic)
            hasher.combine(self.datatype)
            for value in self.values {
                hasher.combine(value.value)
                hasher.combine(value.confidence)
            }
        }
    }
    
    private let data: GlimpseData
    var timestamp: String {
        return self.data.timestamp
    }
    var topic: String {
        return self.data.topic
    }
    var values: [GlimpseData.ValueItem] {
        return self.data.values
    }
    
    init(from glimpse: Glimpse<NEXSensorItem>, timestamp: String) {
        self.data = GlimpseData(from: glimpse, timestamp: timestamp)
    }
    
    init(from data: GlimpseData) {
        self.data = data
    }
    
    init(from decoder: Decoder) throws {
        self.data = try GlimpseData(from: decoder)
    }
    
    public func encode(to encoder: Encoder) throws {
        try data.encode(to: encoder)
    }
    
    enum CodingKeys: String, CodingKey {
        case timestamp
        case topic
        case datatype
        case values
    }
}

internal struct SessionInfo: Codable {
    var sessionId: String
    var deviceId: String?
    let coreVersion: String
    let os: String
    
    private enum CodingKeys: String, CodingKey {
        case sessionId = "session_id"
        case deviceId = "device_id"
        case coreVersion = "core_version"
        case os
    }
    
    init() {
        self.sessionId = UUID().uuidString
        self.deviceId = nil
        self.coreVersion = NECORE_VERSION
        self.os = { () -> String in
#if os(watchOS)
            return "WatchOS \(WKInterfaceDevice.current().systemVersion)"
#elseif os(iOS)
            return "iOS \(UIDevice.current.systemVersion)"
#else
            #error("Unknown Operating System")
            return "AppleUnknown"
#endif
        }()
    }
}

internal struct AudienceResponse: Decodable {
    let audiences: [Membership]?
    
    private enum CodingKeys: String, CodingKey {
        case audiences
    }
    
    init(from decoder: Decoder) throws {
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.audiences = try? decCont.decode([Membership].self, forKey: .audiences)
    }
}
