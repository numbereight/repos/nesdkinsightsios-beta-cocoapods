//
//  NEXInsights.h
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Insights/NEXRecordingConfig.h>
#import <NumberEightCompiled/NumberEightCompiled.h>

typedef void (^NEXInsightsOnStartCallback)(BOOL, NSError* _Nullable);

NS_SWIFT_NAME(Insights)
@interface NEXInsights : NSObject

/**
 * The current recording config in use by Insights.  Defaults to the `defaultConfig`.
 * @see -defaultConfig:
 */
@property(class, readonly, nullable) NEXRecordingConfig* config;

/**
 * Whether data is currently being recorded or not.
 */
@property(class, readonly) BOOL isRecording;

/**
 * Boolean indicating whether Audiences should print errors to the console.
 *
 * @default `true`
 */
@property(class, readwrite) BOOL shouldPrintErrors DEPRECATED_ATTRIBUTE;

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 */
+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
    NS_SWIFT_UNAVAILABLE("This has been overriden in swift.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param config Configuration options. Use this to specify a device id if required.
 */
+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
    NS_SWIFT_UNAVAILABLE("This has been overriden in swift.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param onStart Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                           onStart:(NEXInsightsOnStartCallback _Nullable)onStart
    NS_SWIFT_UNAVAILABLE("This has been overriden in swift.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param config Configuration options. Use this to specify a device id if required.
 * @param onStart Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
                           onStart:(NEXInsightsOnStartCallback _Nullable)onStart
    NS_REFINED_FOR_SWIFT;

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param config Configuration options. Use this to specify a device id if required.
 * @param onStarted Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
                         onStarted:(NEXInsightsOnStartCallback _Nullable)onStarted
    DEPRECATED_MSG_ATTRIBUTE("Please use -startRecordingWithAPIToken:config:onStart instead.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
    DEPRECATED_MSG_ATTRIBUTE("Please use -startRecordingWithAPIToken:config instead.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param config Configuration options. Use this to specify a device id if required.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
    DEPRECATED_MSG_ATTRIBUTE("Please use -startRecordingWithAPIToken:config instead.");

/**
 * Start recording data to be analysed via the NumberEight portal.
 *
 * @param apiToken Token received from NumberEight.start().
 * @param onStarted Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                         onStarted:(NEXInsightsOnStartCallback _Nullable)onStarted
    DEPRECATED_MSG_ATTRIBUTE("Please use -startRecordingWithAPIToken:onStart instead.");

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 * @param parameters An extra JSON representable dictionary that will be stored with the marker.
 * @param error pointer to an error that will be set if parameters are not JSON convertible.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (bool)addMarkerWithName:(NSString* _Nonnull)name
               parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters
                    error:(NSError* _Nullable* _Nullable)error NS_REFINED_FOR_SWIFT;

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 * @param error pointer to an error that will be set if parameters are not JSON convertible.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (bool)addMarkerWithName:(NSString* _Nonnull)name error:(NSError* _Nullable* _Nullable)error;

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (bool)addMarker:(NSString* _Nonnull)name
    NS_SWIFT_UNAVAILABLE("This method is unavailable in Swift.")
        DEPRECATED_MSG_ATTRIBUTE("Please use -addMarkerWithName:error instead.");

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 * @param parameters An extra JSON representable dictionary that will be stored with the marker.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (bool)addMarker:(NSString* _Nonnull)name
       parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters
    NS_SWIFT_UNAVAILABLE("This method is unavailable in Swift.")
        DEPRECATED_MSG_ATTRIBUTE("Please use -addMarkerWithName:parameters instead.");

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 * @param parameters An extra JSON representable dictionary that will be stored with the marker.
 * @param isSessionActivePtr Pointer to a boolean indiciating whether a session is currently active.
 * @param error Pointer to an error that will be set if parameters are not JSON convertible.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (BOOL)addMarker:(NSString* _Nonnull)name
            parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters
    isSessionActivePtr:(BOOL* _Nullable)isSessionActivePtr
                 error:(NSError* _Nullable* _Nullable)error NS_REFINED_FOR_SWIFT;

/**
 * Create a marker at the current time to tag a particular app event with the
 * current user context.
 *
 * Some ideas:
 * @code
 * NSString* item = @"exampleItem";
 * [NEXInsights addMarker:@"screen_viewed"]; // Put inside viewDidLoad()
 * [NEXInsights addMarker:@"in_app_purchase" parameters: @{item : @"3bcadd"}]; // Put inside an
 * in-app
 * // purchase handler
 * [NEXInsights addMarker:@"share"] // Put inside a handler for sharing on social media
 * @endcode
 *
 * This function will trigger an upload if the user is currently connected to Wi-Fi.
 *
 * @param name An identifier for the marker.
 * @param isSessionActivePtr Pointer to a boolean indiciating whether a session is currently active.
 * @param error Pointer to an error that will be set if parameters are not JSON convertible.
 *
 * @return boolean indicating whether a marker was successfully added to a currently recording
 * session.
 */
+ (BOOL)addMarker:(NSString* _Nonnull)name
    isSessionActivePtr:(BOOL* _Nullable)isSessionActivePtr
                 error:(NSError* _Nullable* _Nullable)error;

/**
 * Finishes recording Insights for the current app session.
 * Data will continue to be uploaded after calling this function.
 * To start a new app session, call `startRecording` again.
 *
 * @see -startRecordingWithAPIToken:config:onStart:
 */
+ (void)stopRecording;

/**
 * Pauses any existing Insights recording whilst preserving the current session.
 * Data will continue to be uploaded after calling this function.
 * To resume, call `startRecording` again.
 *
 * @see -startRecordingWithAPIToken:config:onStart:
 */
+ (void)pauseRecording;

/**
 * Updates the current RecordingConfig for Insights.
 */
+ (void)updateConfig:(NEXRecordingConfig* _Nonnull)config NS_SWIFT_NAME(updateConfig(config:));

@end
