//
//  NEXMembership.h
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NEXIABAudience.h"
#import "NEXLivenessState.h"

/**
 * A representation of an audience membership.
 *
 * @tag Membership
 */
NS_SWIFT_NAME(Membership)
@interface NEXMembership : NSObject <NSSecureCoding>

/**
 * NumberEight Audience ID
 * e.g.
 *  - NE-1-1
 *  - NE-8-3
 */
@property(readonly, strong) NSString* _Nonnull ID NS_SWIFT_NAME(id);

/**
 * Human readable name for the NumberEight Audience.
 */
@property(readonly, strong) NSString* _Nonnull name;

/**
 * An extended identifier combining the audience ID with a liveness code.
 * The ID and code are separated by a pipe character '|'.
 *
 * The extended ID is designed to be used instead of the ID when liveness is also desired.
 * This is useful for use cases such as adding audience memberships to ad requests.
 *
 * e.g.
 * - "NE-100-1|L" where 'L' is kNEXLivenessStateLive
 * - "NE-101-2|T" where 'T' is kNEXLivenessStateHappenedToday
 * - "NE-5-3|H" where 'H' is kNEXLivenessStateHabitual
 */
@property(nonatomic, readonly, nonnull) NSString* extendedId;

/**
 * IAB Audience extensions corresponding to the NumberEight audience.
 */
@property(readonly, strong) NSArray<NEXIABAudience*>* _Nonnull iabIds;

@property(readonly) NEXLivenessState liveness;

- (instancetype _Nonnull)init NS_UNAVAILABLE;

- (instancetype _Nonnull)initWithID:(NSString* _Nonnull)ID
                               name:(NSString* _Nonnull)name
                             iabIds:(NSArray<NEXIABAudience*>* _Nonnull)iabIds
                           liveness:(NEXLivenessState)liveness;

/**
 * Returns an instance of NEXMembership from a dictionary.
 * @see `NEXMembership:asDictionary`
 */
+ (NEXMembership* _Nullable)fromDictionary:(NSDictionary* _Nullable)dict;

- (NSString* _Nonnull)description;

/**
 * @name NSSecureCoding
 */
- (instancetype _Nonnull)initWithCoder:(NSCoder* _Nullable)coder;
+ (BOOL)supportsSecureCoding;
- (void)encodeWithCoder:(NSCoder* _Nullable)encoder;

- (BOOL)isEqual:(id _Nullable)object;
- (NSUInteger)hash;
- (NSDictionary* _Nonnull)asDictionary;

+ (NSNumber* _Nullable)livenessStateFromString:(NSString* _Nullable)liveness;

/**
 * Returns the serialized name of the liveness object.
 * e.g. "this_week"
 */
+ (NSString* _Nonnull)livenessToString:(NEXLivenessState)liveness;

/**
 * A single letter representation of the liveness.
 * e.g.
 * - kNEXLivenessStateLive -> 'L'
 * - kNEXLivenessStateHappenedToday -> 'T'
 * - kNEXLivenessStateHappenedThisWeek -> 'W'
 * - kNEXLivenessStateHappenedThisMonth -> 'M'
 * - kNEXLivenessStateHabitual -> 'H'
 */
+ (char)livenessCode:(NEXLivenessState)liveness;

@end
