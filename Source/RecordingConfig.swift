//
//  RecordingConfig.swift
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

public extension RecordingConfig {

    /**
     * The initial delay before uploading the first samples to NumberEight.
     * This should ideally be short enough to trigger at least one upload even if the user
     * does not keep the app open for very long.
     *
     * Default: 3s
     */
    var initialUploadDelay: Interval {
        get {
            return Interval(timeInterval: self.__initialUploadDelay)
        }
        set(newValue) {
            self.__initialUploadDelay = newValue.timeIntervalValue
        }
    }

    /**
     * The interval at which data will be sent to NumberEight after the initial upload.
     *
     * Default: 5m
     */
    var uploadInterval: Interval {
        get {
            return Interval(timeInterval: self.__uploadInterval)
        }
        set(newValue) {
            self.__uploadInterval = newValue.timeIntervalValue
        }
    }
}
