//
//  Membership.swift
//  Insights
//
//  Created by Matthew Paletta on 2021-03-11.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

extension Membership: Codable {
    private enum CodingKeys: CodingKey {
        // TODO: Remove (iab_ids) in 4.0
        case id, name, iab_ids, iabIds, liveness
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.iabIds as NSArray as? [IABAudience], forKey: .iabIds)
        try container.encode(Membership.liveness(toString: self.liveness), forKey: .liveness)
    }

    public required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)

        // Renamed iab_ids -> iabIds to match Android & Unity
        var iabIds = try container.decodeIfPresent([IABAudience].self, forKey: .iabIds)
        if iabIds == nil {
            iabIds = try container.decode([IABAudience].self, forKey: .iab_ids)
        }

        // Liveness is represented as a string.
        // Try and turn it into an NSNumber? using .livenessState
        // This will log and return nil if it cannot turn it into an NEXLivenessState value
        // Try and convert the NSNumber into an livenessState
        let livenessStr = try container.decode(String.self, forKey: .liveness)
        guard let livenessVal = Membership.livenessState(from: livenessStr),
              let livenessObj = NEXLivenessState(rawValue: livenessVal.uintValue) else {
                  NELog.msg("Membership", error: "Failed to parse liveness state from string.", metadata: ["liveness": livenessStr])
                  throw NSError(domain: "Membership", code: 1, userInfo: ["Message": "Failed to parse liveness state."])
              }

        guard let iab = iabIds else {
            NELog.msg("Membership", error: "Failed to parse iab ids.")
            throw NSError(domain: "Membership", code: 2, userInfo: ["Message": "Failed to parse iab ids."])
        }

        self.init(id: id, name: name, iabIds: iab, liveness: livenessObj)
    }
}
