//
//  Recorder.swift
//  Insights
//
//  Created by Oliver Kocsis on 18/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight
import SQLite

internal protocol TableDescriptor {
    static var table: Table { get }
    static var createBlock: (TableBuilder) -> Void { get }
}

internal struct Recorder {
    private static let LOG_TAG = "Recorder"
    private static let MAX_SESSIONDATA_RECORDS = 5_000

    var currentSession: String

    init(session: String, metadata: Data) throws {
        NELog.msg(Recorder.LOG_TAG, verbose: "Creating recorder. New session.")

        currentSession = session

        if Recorder.sessionExists(session) {
            NELog.msg(Recorder.LOG_TAG, debug:
                        "Insights Session already exists, reusing existing metadata.", metadata: ["session": session])
            return
        }

        try updateSessionMeta(metadata).get()
    }

    init(continuing session: String) throws {
        NELog.msg(Recorder.LOG_TAG, verbose: "Creating recorder. Continuing session.")

        if Recorder.sessionExists(session) == false {
            throw RecorderError.sessionNotFound
        }

        currentSession = session
    }

    var count: Int? {
        try? Recorder.count(of: self.currentSession).get()
    }

    static func count(of session: String) -> Swift.Result<Int, Error> {
        return Db.withSessionData { (conn, table, fields) -> Int in
            return
                try conn.scalar(table.filter(fields.session == session)
                                      .count)
        }
    }

    static func listAllSessions() -> Swift.Result<[String], Error> {
        return Db.withSessionData { (conn, table, fields) -> [String] in
            let select =  table.select(distinct: fields.session)

            var sessions: [String] = []
            for row in try conn.prepare(select) {
                sessions.append(row[fields.session])
            }
            return sessions
        }
    }

    static func sessionExists(_ session: String) -> Bool {
        switch sessionMetadata(from: session) {
        case .success:
            return true
        case .failure:
            return false
        }
    }

    private func updateSessionMeta(_ metadata: Data) -> Swift.Result<Void, Error> {
        let buff = metadata.withUnsafeBytes { $0 }
        guard let ptr = buff.baseAddress else {
            return .failure(RecorderError.memoryAccessViolation)
        }
        let blob = Blob(bytes: ptr, length: buff.count)

        return Db.withSessionMeta { (conn, table, fields) in
            let insert = table.insert(fields.session <- self.currentSession,
                                      fields.metadata <- blob)
            try conn.run(insert)
        }
    }

    private static func dataToBlob(_ data: Data) -> Swift.Result<Blob, Error> {
        let buff = data.withUnsafeBytes { $0 }
        guard let ptr = buff.baseAddress else {
            return .failure(RecorderError.memoryAccessViolation)
        }

        let blob = Blob(bytes: ptr, length: buff.count)
        return .success(blob)
    }

    private static func latestStateToBlob(_ state: InsightsState.StateType) -> Swift.Result<Blob, Error> {
        guard let encodedState = try? JSONEncoder().encode(state) else {
            return .failure(RecorderError.decodingJsonData)
        }

        return Recorder.dataToBlob(encodedState)
    }

    func record(_ data: Data, state: InsightsState.StateType, at timestamp: String) -> Swift.Result<Void, Error> {
        NELog.msg(Recorder.LOG_TAG, verbose: "Recording Insights data to database.")
        let dataBlobResult = Recorder.dataToBlob(data)
        let stateBlobResult = Recorder.latestStateToBlob(state)

        // Catch errors here and return early.
        switch dataBlobResult {
        case .failure(let error):
            return .failure(error)
        default:
            // Do nothing
            break
        }

        switch stateBlobResult {
        case .failure(let error):
            return .failure(error)
        default:
            // Do nothing
            break
        }

        return Db.withSessionData { (conn, table, fields) in
            if try conn.scalar(table.count) > Recorder.MAX_SESSIONDATA_RECORDS {
                // Limit the size of the table so if we have duplicate glimpses and/or are unable to
                // upload and therefore clear glimpses because of SQLite errors, the size on
                // disk stops growing.
                throw RecorderError.databaseFull
            }

            let insert = table.insert(fields.session <- self.currentSession,
                                      // Caught errors above
                                      fields.data <- try dataBlobResult.get(),
                                      fields.state <- try stateBlobResult.get(),
                                      fields.timestamp <- timestamp)
            try conn.run(insert)
            NELog.msg(Recorder.LOG_TAG, verbose: "Insights data recorded.")
        }
    }

    func sessionMetadata() -> Swift.Result<Data, Error> {
        return Recorder.sessionMetadata(from: self.currentSession)
    }

    static func sessionMetadata(from session: String) -> Swift.Result<Data, Error> {
        return Db.withSessionMeta { (conn, table, fields) -> Data in
            let select =
                table.select(fields.session,
                             fields.metadata)
                     .filter(fields.session == session)

            let rows = [Row](try conn.prepare(select))
            if rows.count == 0 {
                throw RecorderError.sessionNotFound
            }

            return Data(rows[0][fields.metadata].bytes)
        }
    }

    func playBack(with options: (limit: Int, offset: Int)? = nil,
                  receivedData: ((_ index: Int, _ id: Int64, _ data: Data, _ state: Data) -> Void)? = nil) -> Swift.Result<PlayBackResult, Error> {
        return Recorder.playBack(from: self.currentSession,
                                 with: options,
                                 receivedData: receivedData)
    }

    static func playBack(from session: String,
                         with options: (limit: Int, offset: Int)? = nil,
                         receivedData: ((_ index: Int, _ id: Int64, _ data: Data, _ state: Data) -> Void)? = nil) -> Swift.Result<PlayBackResult, Error> {
        return Db.withSessionData { (conn, table, fields) -> PlayBackResult in
            var select =
                table.select(fields.id,
                             fields.session,
                             fields.data,
                             fields.state)
                .filter(fields.session == session)
                .order(fields.timestamp.asc)
            if let opt = options {
                select =
                    select.limit(opt.limit, offset: opt.offset)
            }
            var i = -1
            for row in try conn.prepare(select) {
                i += 1
                receivedData?(i,
                              row[fields.id],
                              Data(row[fields.data].bytes),
                              Data(row[fields.state].bytes))
            }
            if i == -1 {
                return .empty
            }
            return .lastIndex(i)
        }
    }

    func erase() -> Swift.Result<Void, Error> {
        return Recorder.erase(session: self.currentSession)
    }

    static func erase(session: String) -> Swift.Result<Void, Error> {
        let dataTask = Db.withSessionData { (conn, table, fields) -> Void in
            let deleteQuery =
                table.filter(fields.session == session)
                     .delete()
            try conn.run(deleteQuery)
        }

        if case let .failure(error) = dataTask {
            return .failure(error)
        }

        return Db.withSessionMeta { (conn, table, fields) -> Void in
            let deleteQuery =
                table.filter(fields.session == session)
                     .delete()
            try conn.run(deleteQuery)
        }
    }

    static func erase(ids: [Int64]) -> Swift.Result<Void, Error> {
        return Db.withSessionData { (conn, table, fields) -> Void in
            let deleteQuery =
                table.filter(ids.contains(fields.id))
                     .delete()
            try conn.run(deleteQuery)
        }
    }

    static func eraseAllSessions() -> Swift.Result<Void, Error> {
        let dataTask = Db.withSessionData { (conn, table, _) -> Void in
            try conn.run(table.delete())
        }

        if case let .failure(error) = dataTask {
            return .failure(error)
        }

        return Db.withSessionMeta { (conn, table, _) -> Void in
            try conn.run(table.delete())
        }
    }

    internal enum PlayBackResult: Equatable {
        case empty
        case lastIndex(Int)

        static func == (lhs: PlayBackResult, rhs: PlayBackResult) -> Bool {
            switch (lhs, rhs) {
            case (.empty, .empty):
                return true
            case (let .lastIndex(idx1), let .lastIndex(idx2)):
                return idx1 == idx2
            default:
                return false
            }
        }
    }

    internal enum RecorderError: Error {
        case noConnection
        case sessionNotFound
        case memoryAccessViolation
        case decodingJsonData
        case databaseFull
    }

    fileprivate struct Db {
        static let version: Int32 = 5
        static let name = "insights.db"
        static let tableDescriptors: [TableDescriptor.Type] = [SessionMeta.self, SessionData.self]
        static var location: Connection.Location? = {
            let result =
                Swift.Result { () -> String in
                    try FileManager
                            .default
                            .url(for: .applicationSupportDirectory,
                                 in: .userDomainMask,
                                 appropriateFor: nil,
                                 create: true)
                            .appendingPathComponent(Db.name)
                            .absoluteString
                }
            switch result {
            case let .success(urlStr):
                return .uri(urlStr)
            case let .failure(error):
                NELog.msg(Uploader.LOG_TAG, error: error.localizedDescription)

                return nil
            }
        }()
        static var connection: Connection? = {
            guard let loc = Db.location else {
                return nil
            }
            let result =
                Swift.Result { () -> Connection in
                    try Connection(loc)
                }
            switch result {
            case let .success(conn):
                if Db.version != conn.userVersion {
                    conn.onVersionChange(newVersion: Db.version)
                }
                conn.onCreate()
                return conn
            case let .failure(error):
                NELog.msg(Uploader.LOG_TAG, error: error.localizedDescription)

                return nil
            }
        }()

        static func withSessionMeta<R>(_ with: (
            _ conn: Connection,
            _ table: Table,
            _ fields: SessionMeta.Fields.Type) throws -> R ) -> Swift.Result<R, Error> {
            guard let conn = self.connection else {
                return .failure(RecorderError.noConnection)
            }

            do {
                let data = try with(conn, SessionMeta.table, SessionMeta.Fields.self)
                return .success(data)
            } catch let error {
                return .failure(error)
            }
        }

        static func withSessionData<R>(_ with: (
            _ conn: Connection,
            _ table: Table,
            _ fields: SessionData.Fields.Type) throws -> R ) -> Swift.Result<R, Error> {
            guard let conn = self.connection else {
                return .failure(RecorderError.noConnection)
            }

            do {
                let data = try with(conn, SessionData.table, SessionData.Fields.self)
                return .success(data)
            } catch let error {
                return .failure(error)
            }
        }

        struct SessionMeta: TableDescriptor {
            static var table: Table {
                return Table("sessionmeta")
            }

            static var createBlock: (TableBuilder) -> Void {
                return { (t) in
                    t.column(Fields.session, primaryKey: true)
                    t.column(Fields.metadata)
                }
            }

            // IF YOU CHANGE THESE, UPDATE DATABASE VERSION
            struct Fields {
                static let session = Expression<String>("session")
                static let metadata = Expression<Blob>("metadata")
            }
        }

        class SessionData: TableDescriptor {
            static var table: Table {
                return Table("sessiondata")
            }

            static var createBlock: (TableBuilder) -> Void {
                return { (t) in
                    t.column(Fields.id, primaryKey: true)
                    t.column(Fields.session)
                    t.column(Fields.data)
                    t.column(Fields.state)
                    t.column(Fields.timestamp)
                }
            }

            // IF YOU CHANGE THESE, UPDATE DATABASE VERSION
            struct Fields {
                static let id = Expression<Int64>("id")
                static let session = Expression<String>("session")
                static let data = Expression<Blob>("data")
                static let state = Expression<Blob>("state")
                static let timestamp = Expression<String>("timestamp")
            }
        }
    }
}

fileprivate extension Connection {
    private static let LOG_TAG = "Connection"

    private(set) var userVersion: Int32? {
        get {
            let result =
                Swift.Result { () -> Int32? in
                    guard let v = try scalar("PRAGMA user_version") as? Int64 else {
                        return nil
                    }
                    return Int32(v)
                }
            switch result {
            case let .success(uv):
                return uv
            case let .failure(error):
                NELog.msg(Connection.LOG_TAG, error: "Failed to get user version for connection", metadata: ["error": error.localizedDescription])
                return nil
            }
        }
        set {
            guard let nv = newValue else {
                return
            }
            do {
                try run("PRAGMA user_version = \(nv)")
            } catch {
                NELog.msg(Connection.LOG_TAG, error: "Failed to set user version for connection", metadata: ["error": error.localizedDescription])
            }
        }
    }

    func onCreate() {
        for desc in Recorder.Db.tableDescriptors {
            do {
                try self.run(desc.table.create(ifNotExists: true, block: desc.createBlock))
            } catch {
                NELog.msg(Connection.LOG_TAG, error: "Failed to create connection", metadata: ["error": error.localizedDescription])
            }
        }
    }

    func onVersionChange(newVersion: Int32) {
        for desc in Recorder.Db.tableDescriptors {
            do {
                try self.run(desc.table.drop(ifExists: true))
            } catch {
                NELog.msg(Connection.LOG_TAG, error: "Failed to change versions", metadata: ["error": error.localizedDescription])
            }
        }
        self.userVersion = newVersion
        NELog.msg(Connection.LOG_TAG, info: "SQLite db version changed.", metadata: ["old": self.userVersion ?? 1, "new": newVersion])
    }
}
