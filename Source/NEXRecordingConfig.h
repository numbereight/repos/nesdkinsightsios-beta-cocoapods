//
//  NEXRecordingConfig.h
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @class Configuration options for recording context.
 */
NS_SWIFT_NAME(RecordingConfig)
@interface NEXRecordingConfig : NSObject <NSSecureCoding>

/**
 * Default constructor.  Suitable for most use cases.
 */
- (instancetype _Nonnull)init;

/**
 * Default constructor
 *
 * @param deviceID A unique identifier for the device or user.
 * @param uploadWithWifiOnly If true, uploads will only take place on non-metered connections, i.e.
 Wi-Fi.
 * @param initialUploadDelay The initial delay before uploading the first samples to NumberEight.
 * @param uploadInterval The interval at which data will be sent to NumberEight after the initial
 upload.
 * @param mostProbableOnly Upload only the most probable context of each glimpse.
 * @param topics A set of topics to record. Defaults to basic high-level information suitable for
 most use cases.
 * @param filters A map of filters for advanced uses. The key should be an element from the set of
 topics, and the value should be a valid NumberEight Engine filter string.
 */
- (instancetype _Nullable)initWithDeviceId:(NSString* _Nullable)deviceID
                        uploadWithWifiOnly:(BOOL)uploadWithWifiOnly
                        initialUploadDelay:(NSTimeInterval)initialUploadDelay
                            uploadInterval:(NSTimeInterval)uploadInterval
                          mostProbableOnly:(BOOL)mostProbableOnly
                                    topics:(NSSet<NSString*>* _Nonnull)topics
                                   filters:(NSDictionary<NSString*, NSString*>* _Nonnull)filters;

/**
 * Returns an instance from a JSON string.
 */
+ (instancetype _Nullable)fromJSONString:(NSString* _Nullable)string;

/**
 * Returns an instance of NEXRecordingConfig from a dictionary.
 * @see `NEXRecordingConfig:asDictionary`
 */
+ (NEXRecordingConfig* _Nullable)fromDictionary:(NSDictionary* _Nullable)dict;

/**
 * @name NSSecureCoding
 */
- (instancetype _Nonnull)initWithCoder:(NSCoder* _Nullable)coder;
+ (BOOL)supportsSecureCoding;
- (void)encodeWithCoder:(NSCoder* _Nullable)encoder;

/**
 * Serializes a RecordingConfig to an equivalent NSDictionary for use with a JSONSerialization
 * object.
 */
- (NSDictionary* _Nonnull)asDictionary;
- (BOOL)isEqual:(id _Nullable)object;

/**
 * A unique identifier for the device or user.
 * This could be a random UUID, or an account ID for example.
 * This is required only for analysing habitual behaviour of users.
 *
 * @note If this field is used, all data sent to NumberEight must be treated as
 * personal information, and it should be clearly communicated in a privacy notice.
 *
 * Default: null
 */
@property(atomic, strong, readwrite) NSString* _Nullable deviceId;

/**
 * If true, uploads will only take place on non-metered connections, i.e. Wi-Fi.
 *
 * If a cellular connection is determined to be non-metered, then it may also be used
 * when this flag is set to true.
 *
 * Default: false.
 */
@property(atomic, readwrite) BOOL uploadWithWifiOnly;

/**
 * The initial delay before uploading the first samples to NumberEight.
 * This should ideally be short enough to trigger at least one upload even if the user
 * does not keep the app open for very long.
 *
 * Default: 3s
 */
@property(atomic, readwrite) NSTimeInterval initialUploadDelay NS_REFINED_FOR_SWIFT;

/**
 * The interval at which data will be sent to NumberEight after the initial upload.
 *
 * Default: 5m
 */
@property(atomic, readwrite) NSTimeInterval uploadInterval NS_REFINED_FOR_SWIFT;

/**
 * Upload only the most probable context of each glimpse.
 * If false, the full glimpse of all possible contexts is uploaded.
 */
@property(atomic, readwrite) BOOL mostProbableOnly;

/**
 * A set of topics to record. Defaults to basic high-level information suitable for
 * most use cases.
 *
 * Default:
 *  (kNETopic)Activity,
 *  BatteryLevel,
 *  DevicePosition,
 *  IndoorOutdoor,
 *  HeadphonesWired,
 *  HeadphonesBluetooth
 *  Geo,
 *  Reachability,
 *  Situation,
 *  Place,
 *  Time,
 *  Weather,
 *  VolumeLevelGranularCategory
 */
@property(atomic, readwrite) NSSet<NSString*>* _Nonnull topics;

/**
 * A map of filters for advanced uses. The key should be an element from the set of topics,
 * and the value should be a valid NumberEight Engine filter string.
 *
 * The most common use case is for averaging.
 *
 * For example, "avg:30s" for a 30 second rolling average.
 *
 * @see NEXNumberEight:subscribe:
 *
 * Default: basic smoothing, unique glimpses only.
 */
@property(atomic, readwrite) NSDictionary<NSString*, NSString*>* _Nonnull filters;

/**
 * Creates a default config.  Suitable for most use cases.
 */
@property(class, readonly) NEXRecordingConfig* _Nonnull defaultConfig;

@end
