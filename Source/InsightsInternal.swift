//
//  InsightsInternal.swift
//  Insights
//
//  Created by Oliver Kocsis on 09/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight

#if os(watchOS)
import WatchKit
#endif

// MARK: -
public class InsightsInternal: NSObject {
    static let instance = InsightsInternal()

    @objc
    public static var shouldPrintErrors = true

    private var config: RecordingConfig?

    private let numberEight = NumberEight()
    private static let LOG_TAG = "InsightsInternal"

    internal let defaults: UserDefaults
    private var apiToken: APIToken?

    private var timer: MyTimer?
    private var heartbeatTimer: MyTimer?
    
    private var _loggerMutex = PThreadMutex(type: .normal)
    private var _logger: InsightsLogger?
    internal private(set) var logger: InsightsLogger? {
        get {
            return self._loggerMutex.sync { self._logger }
        }
        set {
            self._loggerMutex.sync {
                self._logger = newValue
            }
        }
    }
    
    private let uploader: Uploader = Uploader()
    private var isRegisteredAsConsentChangeListener = false

    @objc
    public static var isRecording: Bool {
        return self.instance.logger != nil
    }

    // Public accessor
    @objc
    public static var config: RecordingConfig? {
        return self.instance.config
    }
    
    private override init() {
        // swiftlint:disable force_unwrapping
        self.defaults = UserDefaults(suiteName: "ai.numbereight.insights")!
        // swiftlint:enable force_unwrapping

        super.init()
        NumberEight.add(dataProvider: self)
    }

    // Used for testing
    internal init(config: RecordingConfig) {
        self.config = config

        // swiftlint:disable force_unwrapping
        self.defaults = UserDefaults(suiteName: "ai.numbereight.insights")!
        // swiftlint:enable force_unwrapping

        super.init()
    }

    func _initializeRecording(apiToken: APIToken, config: RecordingConfig) {
        // Store local varibles, so we can start recording when we later receive consent
        // No processing should happen in this function.
        if !self.isRegisteredAsConsentChangeListener {
            NumberEight.add(onConsentChangeListener: self)
            self.isRegisteredAsConsentChangeListener = true
        }

        self.apiToken = apiToken
        self.config = config
    }

    @objc
    public static func updateConfig(config: RecordingConfig) {
        InsightsInternal.instance.config = config
        InsightsInternal.instance.logger?.updateConfig(config: config)
    }

    @objc
    public static func startRecording(apiToken: APIToken, config: RecordingConfig, onStart: @escaping (_ isSuccess: Bool, _ error: Error?) -> Void) {
        InsightsInternal.instance._startRecording(apiToken: apiToken, config: config) { result in
            switch result {
            case .success:
                onStart(true, nil)
            case let .failure(error):
                onStart(false, error)
            }
        }
    }

    internal func _startRecording(apiToken: APIToken,
                                  config: RecordingConfig = .default,
                                  onStart: ((Result<Void, Error>) -> Void)? = nil) {
        NELog.msg(InsightsInternal.LOG_TAG, verbose: "Starting Recording.")

        guard InsightsInternal.isRecording == false else {
            onStart?(.success(Void()))
            return
        }

        let consentErrorCode: Int = 456
        let callback = onStart ?? { (result: Result<Void, Error>) -> Void in
            switch result {
            case .success:
                NELog.msg(InsightsInternal.LOG_TAG,
                          info: "NumberEight Insights started successfully")
            case let .failure(error):
                if (error as NSError).code == consentErrorCode {
                    NELog.msg(InsightsInternal.LOG_TAG,
                              warning: "NumberEight Insights failed to start.", metadata: ["error": error.localizedDescription])
                } else {
                    // Add other failure states here, as errors, as appropriate.
                    NELog.msg(InsightsInternal.LOG_TAG,
                              error: "NumberEight Insights failed to start.", metadata: ["error": error.localizedDescription])
                }
            }
        }

        // We have to register first in case we don't have consent at the beginning.
        self._initializeRecording(apiToken: apiToken, config: config)
        if !NumberEight.consentOptions().hasRequiredConsent(self) {
            callback(.failure(
                        NSError(domain: "NumberEight Insights does not have the appropriate consent required to run.",
                                code: consentErrorCode, userInfo: nil)))
            return
        }

        var logger: InsightsLogger
        do {
            logger = try self._newSession(sessionInfo: SessionInfo())
            self.logger = logger
        } catch let error {
            callback(.failure(error))
            self.config = nil
            return
        }

        AppLifeCycleProxy.start(proxyTo: InsightsInternal.self)

        var sessionInfo = SessionInfo()
        sessionInfo.sessionId = logger.currentSession
        sessionInfo.deviceId = config.deviceId

        Uploader.check(sessionInfo: sessionInfo) { [weak self] (result) in
            switch result {
            case .success:
                self?.setUpSubscriptions(config: config)
                self?.scheduleUploads(delay: config.initialUploadDelay,
                                      interval: config.uploadInterval,
                                      onWifiOnly: config.uploadWithWifiOnly)

                self?.heartbeatTimer = MyTimer.scheduled(deadline: .now(),
                                                         repeating: .seconds(15),
                                                         leeway: .milliseconds(100)) { [weak self] in
                    let timestamp = MicrosecondPrecisionDateFormatter().string(from: Date())
                    self?.logger?.addHeartbeat(timestamp: timestamp)
                }

                callback(.success(()))
            case let .failure(error):
                self?._rollback(logger: &logger)
                self?.config = nil
                self?.logger = nil
                callback(.failure(error))
            }
        }
    }

    private func endPreviousSession() {
        let lastSessionOpt = self.defaults.string(forKey: "lastSession")
        let lastHeartbeatOpt = self.defaults.string(forKey: "lastHeartbeat")

        guard let lastSession = lastSessionOpt, let lastHeartbeat = lastHeartbeatOpt else {
            NELog.msg(InsightsInternal.LOG_TAG, error: "Failed to get lastSession or lastHeartbeat while trying to end session. Cannot end session.",
                      metadata: ["hasLastSession": lastSessionOpt != nil, "hasLastHeartbeat": lastHeartbeatOpt != nil])
            return
        }

        let result = self.logger?.logLifecycle(session: lastSession, name: "end", timestamp: lastHeartbeat)
        switch result {
        case .failure:
            NELog.msg(InsightsInternal.LOG_TAG, error: "Previous session not found, removing.", metadata: ["lastSession": lastSession])
            switch Recorder.erase(session: lastSession) {
            case .success:
                break
            case let .failure(error):
                NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to erase session with error.", metadata: ["error": error.localizedDescription])
            }
        default:
            break
        }
    }

    private func _newSession(sessionInfo: SessionInfo) throws -> InsightsLogger {
        self.endPreviousSession()

        let logger = try InsightsLogger.makeLogger(forSession: sessionInfo, defaults: self.defaults, config: self.config)

        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        switch logger.logLifecycle(name: "start", timestamp: now) {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to log life lifecycle (start) with error.", metadata: ["error": error.localizedDescription])
        }
        self.defaults.set(sessionInfo.sessionId, forKey: "lastSession")

        return logger
    }

    func _endSession() {
        self.defaults.set(nil, forKey: "lastSession")
        self.defaults.set(nil, forKey: "lastHeartbeat")

        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        switch self.logger?.logLifecycle(name: "end", timestamp: now) {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to log life lifecycle (end) with error.", metadata: ["error": error.localizedDescription])
        case .none:
            // No logger yet, not an error
            break
        }
    }

    func _rollback(logger: inout InsightsLogger) {
        switch logger.erase() {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to erase session data during rollback with error.", metadata: ["error": error])
        }

        let lastSession = self.defaults.string(forKey: "lastSession")
        if lastSession == self.logger?.currentSession {
            self.defaults.set(nil, forKey: "lastSession")
            self.defaults.set(nil, forKey: "lastHeartbeat")
        }
    }

    @objc
    public static func addMarker(name: String, parameters: [String: Any] = [:], isSessionActivePtr: UnsafeMutablePointer<Bool>?) throws {
        let result = InsightsInternal.instance.logger?.logMarker(name: name, parameters: parameters)
        switch result {
        case .success:
            isSessionActivePtr?.pointee = true
        case let .failure(error):
            throw error
        case .none:
            isSessionActivePtr?.pointee = false
        }
    }

    /**
     * Returns the count of data in the session, or -1 if the session cannot be found.
     */
    public func sizeOfSession(session: String) -> Int {
        return InsightsLogger.sizeOfSession(session: session)
    }

    @objc
    public static func pauseRecording() {
        InsightsInternal.instance._pauseRecording()
    }

    func _pauseRecording() {
        NELog.msg(InsightsInternal.LOG_TAG, verbose: "Pausing recording.")
        self.numberEight.unsubscribeFromAll()
        self.heartbeatTimer = nil

        AppLifeCycleProxy.stop()

        self._endSession()

        self.logger = nil
    }

    @objc
    public static func stopRecording() {
        InsightsInternal.instance._stopRecording()
    }

    public func _stopRecording() {
        NELog.msg(InsightsInternal.LOG_TAG, verbose: "Stopping recording.")

        NumberEight.remove(onConsentChangeListener: self)
        self.isRegisteredAsConsentChangeListener = false

        self.numberEight.unsubscribeFromAll()
        self.timer = nil
        self.heartbeatTimer = nil

        AppLifeCycleProxy.stop()

        self._endSession()
        self.logger = nil
        self.config = nil
    }
}

// MARK: - Data Provider
extension InsightsInternal: DataProvider {
    public func performDataRemoval() {
        NELog.msg(InsightsInternal.LOG_TAG, debug: "Insights is deleting its data, stopping.")
        let wasRecording = InsightsInternal.isRecording

        // StopRecording removes the config, so we need to store this here before.
        let beforeToken = self.apiToken
        let beforeConfig = self.config

        if wasRecording {
            self._stopRecording()
        }

        self.timer = nil
        switch Recorder.eraseAllSessions() {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to erase all sessions as part of data removal.", metadata: ["error": error.localizedDescription])
        }
        self.clearUserDefaults()

        if wasRecording {
            guard let token = beforeToken, let conf = beforeConfig else { return }
            self._startRecording(apiToken: token, config: conf, onStart: nil)
            NELog.msg(InsightsInternal.LOG_TAG, debug: "Data deleted, restarting.")
        }

        NELog.msg(InsightsInternal.LOG_TAG, info: "Data deleted.")
    }

    private func clearUserDefaults() {
        self.defaults.dictionaryRepresentation().keys.forEach { key in
            self.defaults.removeObject(forKey: key)
        }
        self.defaults.synchronize()
    }
}

// MARK: - Consentable
extension InsightsInternal: Consentable {
    public var requiredConsent: [NSNumber]! {
        let req: [ConsentOptionsProperty] = [.ALLOW_PROCESSING, .ALLOW_STORAGE,
                                             .ALLOW_USE_FOR_REPORTING]
        return (req.map({ $0.rawValue}) as [NSNumber])
    }
}

// MARK: - Consent Change Listener
extension InsightsInternal: OnConsentChangeListener {
    public func consentDidChange(_ consentOptions: ConsentOptions) {
        if consentOptions.hasRequiredConsent(self) {
            guard let prevToken = self.apiToken, let prevConfig = self.config else {
                NELog.msg(InsightsInternal.LOG_TAG, error: "Failed to get prevToken or preConfig.  Cannot update Insights with new consent.")
                return
            }

            // We must have started before, start again.
            // Wait for the completion before moving on
            let group = DispatchGroup()
            group.enter()
            Insights.startRecording(apiToken: prevToken, config: prevConfig) { _ in
                group.leave()
            }
            group.wait()
            NELog.msg(InsightsInternal.LOG_TAG, info: "NumberEight Insights now has consent to run, resuming.")
        } else {
            Insights.pauseRecording()
            NELog.msg(InsightsInternal.LOG_TAG, info:
                        "NumberEight Insights no longer has appropriate consent, pausing.")
        }
    }
}

extension InsightsInternal: AppLifeCycleObserver {
    static func willEnterForeground(_ notification: NSNotification) {
        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        switch InsightsInternal.instance.logger?.logLifecycle(name: "app_in_foreground", timestamp: now) {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to insert lifecycle marker (app_in_foreground) with error.", metadata: ["error": error.localizedDescription])
        case .none:
            // No logger yet, not an error
            break
        }
    }

    static func didEnterBackground(_ notification: NSNotification) {
        let now = MicrosecondPrecisionDateFormatter.shared.string(from: Date())
        switch InsightsInternal.instance.logger?.logLifecycle(name: "app_in_background", timestamp: now) {
        case .success:
            break
        case let .failure(error):
            NELog.msg(InsightsInternal.LOG_TAG, warning: "Failed to insert lifecycle marker (app_in_background) with error.", metadata: ["error": error.localizedDescription])
        case .none:
            // No logger yet, not an error
            break
        }
    }
}

// MARK: - Internal helper funtions -
internal extension InsightsInternal {
    private func setUpSubscriptions(config: RecordingConfig) {
        for topic in config.topics {
            NELog.msg(InsightsInternal.LOG_TAG, verbose: "Setting up subscriptions for topic.", metadata: ["topic": topic])

            var filter = config.filters[topic]

            // If using mostProbableOnly with a unique filter, force the firstValueOnly flag to true
            if config.mostProbableOnly {
                filter = filter?.replacingOccurrences(
                    of: #"(uniq(?:ue)?)(?::(?:\d+(,\w+)?)?)?"#,
                    with: "$1:1$2",
                    options: .regularExpression
                ).replacingOccurrences(
                    of: #"(hyst(?:eresis)?):(\d+(?:h|m|s|ms|us|μs|ns))(?:,\d+(,\w+)?)?"#,
                    with: "$1:$2,1$3",
                    options: .regularExpression)
            }
            var params: Parameters?
            if let filter = filter {
                params = Parameters(filter: filter)
            }

            self.numberEight.subscribe(to: topic,
                                       parameters: params) { (glimpse: Glimpse<NEXSensorItem>) in
                self.logger?.logEvent(topic: topic, glimpse: glimpse)
            }
        }
    }

    private func performUpload(onWifiOnly: Bool) {
        if self.apiToken == nil {
            NELog.msg(InsightsInternal.LOG_TAG, error: "Insights cannot upload: no API token is set.")
            return
        }

        _ = self.numberEight.request(forTopic: kNETopicReachability, parameters: nil) { (gl: Glimpse<NEXReachability>) in
            let reachability = gl.mostProbable
            let isMetered = reachability.isMetered()
            if onWifiOnly && isMetered {
                NELog.msg(InsightsInternal.LOG_TAG, debug: "Was scheduled to upload, but skipping.", metadata: ["onWifiOnly": onWifiOnly, "isMetered": isMetered])
                return
            }

            if reachability.hasInternet() {
                self.uploader.uploadAll(currentSession: self.logger?.currentSession, lowDataMode: isMetered)
            } else {
                NELog.msg(InsightsInternal.LOG_TAG, debug: "Was scheduled to upload, but skipping because we are not connected to the internet.")
            }
        }
    }

    private func scheduleUploads(delay: Interval, interval: Interval, onWifiOnly: Bool) {
        self.timer = MyTimer.scheduled(deadline: .now() + delay.dispatchTimeIntervalValue,
                                       repeating: interval.dispatchTimeIntervalValue) {
            self.performUpload(onWifiOnly: onWifiOnly)
        }

        if onWifiOnly {
            self.numberEight.unsubscribe(from: kNETopicReachability)
        }
    }

}

// MARK: - Helper extensions -
public extension NumberEight {
    class var `Insights`: Insights.Type {
        typealias NEInsights = Insights
        return NEInsights.self
    }
}

// MARK: - Temporary Audiences extensions -
public class AudiencesExtension {
    public static private(set) var sharedInstance = AudiencesExtension()

    private let defaults: UserDefaults
    private var _audienceMemberships: Set<Membership>?

    private static let LOG_TAG = "AudiencesExtension"

    init() {
        // swiftlint:disable force_unwrapping
        self.defaults = UserDefaults(suiteName: "ai.numbereight.audiences")!
        // swiftlint:enable force_unwrapping
    }

    public internal(set) var audienceMemberships: Set<Membership> {
        get {
            if let memberships = _audienceMemberships {
                return memberships
            }

            // Have to store custom objects as JSON
            do {
                guard let storedObjItem = self.defaults.object(forKey: "memberships") as? Data else {
                    return Set()
                }
                let storedItems = try JSONDecoder().decode([Membership].self, from: storedObjItem)
                return Set(storedItems)
            } catch let error {
                NELog.msg(AudiencesExtension.LOG_TAG,
                          error: "Error while decoding stored Audience memberships", metadata: ["error": error.localizedDescription])
            }

            return Set()
        }
        set {
            // Have to store custom objects as JSON
            if let encoded = try? JSONEncoder().encode(newValue) {
                self.defaults.set(encoded, forKey: "memberships")
            }
            _audienceMemberships = newValue
        }
    }

    public func _initializeRecording(apiToken: APIToken, config: RecordingConfig) {
        InsightsInternal.instance._initializeRecording(apiToken: apiToken, config: config)
    }
}

extension InsightsInternal {
    public static var ext: AudiencesExtension {
        return AudiencesExtension.sharedInstance
    }
}
