//
//  Utils.swift
//  Insights
//
//  Created by Oliver Kocsis on 09/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight
import Dispatch

internal struct AtomicBool {
    private var semaphore = DispatchSemaphore(value: 1)
    private var value: Bool = false
    var val: Bool {
        get {
            self.semaphore.wait()
            let tmp = self.value
            self.semaphore.signal()
            return tmp
        }
        set {
            self.semaphore.wait()
            self.value = newValue
            self.semaphore.signal()
        }
    }
}

internal final class MicrosecondPrecisionDateFormatter: DateFormatter {
    static var shared = MicrosecondPrecisionDateFormatter()

    override func string(from date: Date) -> String {

        self.locale = Locale(identifier: "en_US_POSIX")
        self.dateFormat = "Z"

        let timeZoneString = super.string(from: date)
        let idx = timeZoneString.index(timeZoneString.startIndex, offsetBy: 2)
        let substr = timeZoneString[...idx]

        let components = calendar.dateComponents(in: self.timeZone, from: date)

        let nanosecondsInMicrosecond = Double(1000)
        let nanosecond = components.nanosecond ?? 0
        let microseconds = lrint(Double(nanosecond) / nanosecondsInMicrosecond)

        let string = String(format: "%04ld-%02ld-%02ldT%02ld:%02ld:%02ld.%06ld%@:00",
                            components.year ?? 0,
                            components.month ?? 0,
                            components.day ?? 0,
                            components.hour ?? 0,
                            components.minute ?? 0,
                            components.second ?? 0,
                            microseconds,
                            String(substr))

        return string
    }

    override func date(from string: String) -> Date? {
        self.locale = Locale(identifier: "en_US_POSIX")
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZ"

        guard let microsecondsPrefixRange = string.range(of: ".") else { return nil }
        let microsecondsWithTimeZoneString = string[microsecondsPrefixRange.upperBound...]

        let nonDigitsCharacterSet = CharacterSet.decimalDigits.inverted
        guard
            let timeZoneRangePrefixRange =
                microsecondsWithTimeZoneString.rangeOfCharacter(from: nonDigitsCharacterSet)
            else { return nil }

        let microsecondsString =
            microsecondsWithTimeZoneString[timeZoneRangePrefixRange.lowerBound...]
        guard
            let microsecondsCount = Double(microsecondsString)
            else { return nil }

        let dateStringExludingMicroseconds = string
            .replacingOccurrences(of: microsecondsString, with: "")
            .replacingOccurrences(of: ".", with: "")

        guard let date = super.date(from: dateStringExludingMicroseconds) else { return nil }
        let microsecondsInSecond = Double(1000000)
        let dateWithMicroseconds = date + microsecondsCount / microsecondsInSecond

        return dateWithMicroseconds
    }
}

public enum Interval {
    case nanoseconds(_: Int)
    case microseconds(_: Int)
    case milliseconds(_: Int)
    case minutes(_: Int)
    case seconds(_: Double)
    case hours(_: Int)
    case days(_: Int)

    init(timeInterval: TimeInterval) {
        self = .seconds(timeInterval)
    }

    internal var dispatchTimeIntervalValue: DispatchTimeInterval {
        switch self {
        case .nanoseconds(let value):        return .nanoseconds(value)
        case .microseconds(let value):       return .microseconds(value)
        case .milliseconds(let value):       return .milliseconds(value)
        case .seconds(let value):            return .milliseconds(Int( Double(value) * Double(1000)))
        case .minutes(let value):            return .seconds(value * 60)
        case .hours(let value):              return .seconds(value * 3600)
        case .days(let value):               return .seconds(value * 86400)
        }
    }

    internal var timeIntervalValue: TimeInterval {
        switch self {
        case .nanoseconds(let value):        return TimeInterval(value) / (1000 * 1000 * 1000)
        case .microseconds(let value):       return TimeInterval(value) / (1000 * 1000)
        case .milliseconds(let value):       return TimeInterval(value) / 1000
        case .seconds(let value):            return value
        case .minutes(let value):            return TimeInterval(value * 60)
        case .hours(let value):              return TimeInterval(value * 60 * 60)
        case .days(let value):               return TimeInterval(value * 60 * 60 * 24)
        }
    }
}

class MyTimer: Equatable {

    enum State: Equatable {
        case suspended
        case resumed

        public static func == (lhs: State, rhs: State) -> Bool {
            switch (lhs, rhs) {
            case (.suspended, .suspended),
                 (.resumed, .resumed):
                return true
            default:
                return false
            }
        }
    }

    /// Current state of the timer
    private(set) var state: State

    func setEventHandler(_ handler: @escaping () -> Void) {
        self.timer.setEventHandler(handler: handler)
    }

    /// Internal GCD Timer
    private var timer: DispatchSourceTimer

    private var queue: DispatchQueue

    init(deadline: DispatchTime,
         repeating: DispatchTimeInterval,
         leeway: DispatchTimeInterval = .nanoseconds(0),
         flags: DispatchSource.TimerFlags = [],
         queue: DispatchQueue? = nil) {
        self.state = .suspended
        self.queue = (queue ?? DispatchQueue(label: "com.repeat.\(NSUUID().uuidString)"))

        let timer = DispatchSource.makeTimerSource(flags: flags,
                                                   queue: self.queue)

        timer.schedule(deadline: deadline,
                       repeating: repeating,
                       leeway: leeway)

        self.timer = timer
    }

    public class func scheduled(deadline: DispatchTime,
                                repeating: DispatchTimeInterval,
                                leeway: DispatchTimeInterval = .nanoseconds(0),
                                queue: DispatchQueue? = nil,
                                handler: @escaping () -> Void) -> MyTimer {
        let myTimer = MyTimer(deadline: deadline,
                              repeating: repeating,
                              leeway: leeway,
                              queue: queue)
        myTimer.setEventHandler(handler)
        myTimer.resume_s()
        return myTimer
    }

    func resume_s() {
        guard state != .resumed else {
            return
        }
        self.state = .resumed
        self.timer.resume()
    }

    func suspend_s() {
        guard state != .suspended else {
            return
        }
        state = .suspended
        self.timer.suspend()
    }

    deinit {
        timer.setEventHandler(handler: nil)
        timer.cancel()
        self.resume_s()
    }

    public static func == (lhs: MyTimer, rhs: MyTimer) -> Bool {
        return lhs === rhs
    }
}
