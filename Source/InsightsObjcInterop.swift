//
//  InsightObjcInterop.swift
//  Insights
//
//  Created by Oliver Kocsis on 06/12/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight

public extension Insights {
    /**
     Start recording data to be analysed via the NumberEight portal.

     - Parameters:
     - apiToken: Token received from NumberEight.start().
     - config: Configuration options. Use this to specify a device id if required.
     - onStart: Optional callback for determining whether recording started successfully.
     */
    static func startRecording(apiToken: APIToken,
                               config: RecordingConfig = .default,
                               onStart: ((Result<Void, Error>) -> Void)? = nil) {
        let callback: NEXInsightsOnStartCallback = { (isSuccess: Bool, _ errorOrNil: Error?) -> Void in
            guard let c = onStart else { return }

            if let error = errorOrNil, !isSuccess {
                c(Result.failure(error))
            } else {
                c(Result.success(Void()))
            }
        }

        Insights.__startRecording(with: apiToken,
                                  config: config,
                                  onStart: callback)
    }

    /**
     * Create a marker at the current time to tag a particular app event with the
     * current user context.

     * Some ideas:
     * ```
     * let item = "exampleItem"
     * Insights.addMarker("screen_viewed") // Put inside viewDidLoad()
     * Insights.addMarker("in_app_purchase", parameters: [item : "3bcadd"]) // Put inside an in-app
     * // purchase handler
     * Insights.addMarker("share") // Put inside a handler for sharing on social media
     * ```
     *
     * This function will trigger an upload if the user is currently connected to Wi-Fi.
     *
     * - Parameters:
     *  - name An identifier for the marker.
     *  - parameters An extra JSON representable dictionary that will be stored with the marker.
     *  - error Pointer to an error that will be set if parameters are not JSON convertible.
     *
     * - Returns boolean indicating whether a marker was successfully added to a currently recording session.
     */
    @discardableResult
    static func addMarker(_ name: String, parameters: [String: Any] = [:]) -> Result<Bool, Error> {
        let error: NSErrorPointer = nil
        let result: Bool = Insights.__addMarker(withName: name, parameters: parameters, error: error)
        if let e = error?.pointee {
            return .failure(e)
        } else {
            return .success(result)
        }
    }

}
